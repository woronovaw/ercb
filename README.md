# Exchange Rate Currency Bot

### Development

Uses the default Django development server.

1. Rename *.env.sample* to *.env*.
1. Update the environment variables in the *docker-compose.yml* and *.env* files.
1. Build the images and run the containers:

    ```sh
    $ docker-compose up -d --build
    ```

    Test it out at [http://localhost:8000](http://localhost:8000). The "app" folder is mounted into the container and your code changes apply automatically.
