import json
import logging

import telebot

from django.http import JsonResponse
from django.views import View
from django.conf import settings

from .scraper import get_exchange
from .models import TelegramUser


logger = logging.getLogger('herokulogger')
CODE_NAME = [(code, name) for code, _, name in settings.CURRENCY_DATA]
bot = telebot.TeleBot(settings.BOT_TOKEN)


@bot.message_handler(commands=['start'])
def handle_start(message):
    telegram_user = message.from_user
    telegram_user_dict = telegram_user.to_dict()
    id = telegram_user_dict.pop('id')
    obj, created = TelegramUser.objects.update_or_create(
        id=id,
        defaults=telegram_user_dict,
    )
    changed = 'Created' if created else 'Updated'
    logger.info('%s telegramuser %s', changed, obj)
    bot.reply_to(
        message,
        f"""Привет, {telegram_user.first_name}!
        Я ExchangeRateBot,
        пока я могу показывать только курс НБУ.
        Для помощи введите команду '/help'.""",
    )


@bot.message_handler(commands=['help'])
def handle_help(message):
    bot.reply_to(
        message,
        """Для того чтобы узнать курс НБУ
        на определенную дату
        введите сообщение с кодом валюты
        и дату в формате "YYYYMMDD",
        например, "USD 20210101",
        если ввести только код валюты,
        то будет использована текущая дата.
        Узнать код можно командой "/codes".""",
    )


@bot.message_handler(commands=['codes'])
def handle_codes(message):
    msg = ''
    for code, name in CODE_NAME:
        msg += f'{code} - {name}\n'
    bot.reply_to(
        message,
        f'{msg}',
    )


@bot.message_handler(func=lambda message: True, content_types=['text'])
def handle_echo_message(message):
    message_list = message.text.split()
    code = message_list[0]
    date = None
    if len(message_list) > 1:
        date = message_list[1]

    bot.reply_to(message, get_exchange(code, date))


class BotView(View):
    def post(self, request, *args, **kwargs):
        json_string = json.loads(request.body)
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return JsonResponse({"ok": "POST request processed"})

    def get(self, request, *args, **kwargs):
        bot.remove_webhook()
        bot.set_webhook(url=request.build_absolute_uri())
        return JsonResponse({"ok": "GET request processed"})
