from django.db import models


class TelegramUser(models.Model):
    id = models.BigIntegerField(primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, null=True)
    username = models.CharField(max_length=255, null=True)
    language_code = models.CharField(max_length=255, null=True)
    is_bot = models.BooleanField(default=False)
    can_join_groups = models.BooleanField(null=True)
    can_read_all_group_messages = models.BooleanField(null=True)
    supports_inline_queries = models.BooleanField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Telegram User'
        verbose_name_plural = 'Telegram Users'

    def __str__(self) -> str:
        return self.first_name
