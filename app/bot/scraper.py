import os
import logging
from datetime import datetime
from typing import List, Optional

import requests

from django.conf import settings


logger = logging.getLogger('herokulogger')


URL = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchangenew?json'
CODE_DICT = {code: ratio for code, ratio, _ in settings.CURRENCY_DATA}
proxyDict = {
    'http': os.environ.get('FIXIE_URL', ''),
    'https': os.environ.get('FIXIE_URL', '')
}


def get_exchange(
    valcode: Optional[str],
    exchange_date: Optional[str] = None,
) -> List[dict]:
    url = URL
    if not validate_code(valcode):
        return 'Invalide currency code'

    url = f'{url}&valcode={valcode}'

    if exchange_date:
        if not validate_date(exchange_date):
            return 'Format date is wrong.'
        url = f'{url}&date={exchange_date}'

    response = requests.get(url, proxies=proxyDict)
    if not response.ok:
        logger.error(response)
        return 'Something went wrong.'

    response_data = response.json()[0]
    return (
        f'1 {response_data["txt"]} - '
        f'{response_data["rate"]} грн '
        f'курс НБУ на {response_data["exchangedate"]}'
    )


def validate_code(code: str) -> bool:
    return code and CODE_DICT.get(code.upper())


def validate_date(date: str) -> bool:
    try:
        datetime.strptime(date, '%Y%m%d')
    except ValueError:
        return False
    return True
