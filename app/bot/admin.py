from django.contrib import admin

from .models import TelegramUser


@admin.register(TelegramUser)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'created_at')
    readonly_fields = [field.name for field in TelegramUser._meta.fields]
