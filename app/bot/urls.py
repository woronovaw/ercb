from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from .views import BotView


app_name = "bot"
urlpatterns = [
    path(
        f'{settings.BOT_TOKEN.split(":")[1]}/',
        csrf_exempt(BotView.as_view()),
        name="index",
    ),
]
